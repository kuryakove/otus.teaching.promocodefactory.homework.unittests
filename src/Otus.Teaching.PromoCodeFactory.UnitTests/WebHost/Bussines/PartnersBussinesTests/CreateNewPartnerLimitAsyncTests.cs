﻿using AutoFixture.Xunit2;
using System;
using System.Linq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Initialization.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Bussines;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Bussines.PartnersBussinesTests
{
    public class CreateNewPartnerLimitAsyncTests
    {
        [Theory]
        [AutoDomainData]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerGetsLimit_NumberIssuedPromoCodesShouldBeZero(
            [Frozen] Mock<IRepository<Partner>> partnersRepositoryMock,
            SetPartnerPromoCodeLimitRequest request,
            PartnersBussines partnersBussines)
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            Partner partner = new PartnerBuilder().AddLimit().Build();

            partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            await partnersBussines.CreateNewPartnerLimitAsync(partner, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
            partner.PartnerLimits.FirstOrDefault().CancelDate.Should().HaveValue()
                .And.BeCloseTo(DateTime.Now, 100);
        }

        [Theory]
        [AutoDomainData]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerHasNoLimit_NumberIssuedPormocodesShouldNotChange(
            [Frozen] Mock<IRepository<Partner>> partnersRepositoryMock,
            SetPartnerPromoCodeLimitRequest request,
            PartnersBussines partnersBussines)
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            Partner partner = new PartnerBuilder().AddIssuedPromocodes().Activate().Build();

            var numberIssuedPromocodes = partner.NumberIssuedPromoCodes;

            partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            await partnersBussines.CreateNewPartnerLimitAsync(partner, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(numberIssuedPromocodes);
        }

    }
}
