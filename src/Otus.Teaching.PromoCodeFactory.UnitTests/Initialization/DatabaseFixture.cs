﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Initialization
{
    public class DatabaseFixture : IDisposable
    {
        private SqliteConnection _connection;

        public DbContextOptions<DataContext> Options { get; set; }

        public DatabaseFixture()
        {
            _connection = new SqliteConnection("Filename=:memory:");
            _connection.Open();

            Options = new DbContextOptionsBuilder<DataContext>()
                            .UseSqlite(_connection)
                            .Options;

            using var context = new DataContext(Options);

            context.Database.EnsureCreated();
        }

        public void Dispose() => _connection.Dispose();
    }
}
