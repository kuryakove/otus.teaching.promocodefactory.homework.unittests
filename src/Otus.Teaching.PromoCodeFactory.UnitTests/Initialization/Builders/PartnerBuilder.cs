﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Initialization.Builders
{
    public class PartnerBuilder
    {
        private Partner _partner;

        public PartnerBuilder()
        {
            _partner = new Partner()
            {
                Id = Guid.NewGuid(),
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
        }

        public PartnerBuilder AddName(string name = "Игрушки")
        {
            _partner.Name = name;
            return this;
        }

        public PartnerBuilder Activate()
        {
            _partner.IsActive = true;
            return this;
        }

        public PartnerBuilder AddLimit()
        {
            var limit = new PromoCodeLimitBuilder()
                .AddPartner(_partner.Id)
                .AddLimit()
                .AddEndDate()
                .Build();

            _partner.PartnerLimits.Add(limit);

            return this;
        }

        public PartnerBuilder AddIssuedPromocodes(int issuedPromocodes = 10)
        {
            _partner.NumberIssuedPromoCodes = issuedPromocodes;

            return this;
        }

        public Partner Build()
        {
            return _partner;
        }
    }
}
