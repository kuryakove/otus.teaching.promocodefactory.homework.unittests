﻿using System;
using System.Linq;
using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.UnitTests.Initialization;
using Otus.Teaching.PromoCodeFactory.UnitTests.Initialization.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Bussines;
using Otus.Teaching.PromoCodeFactory.WebHost.Bussines.Interfaces;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<DatabaseFixture>
    {
        private readonly DatabaseFixture _databaseFixture;

        public SetPartnerPromoCodeLimitAsyncTests(DatabaseFixture databaseFixture)
        {
            _databaseFixture = databaseFixture;
        }

        [Theory]
        [AutoDomainData]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound(
            [Frozen] Mock<IRepository<Partner>> partnersRepositoryMock,
            SetPartnerPromoCodeLimitRequest request,
            [NoAutoProperties] PartnersController partnersController)
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            Partner partner = null;

            partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory]
        [AutoDomainData]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnBadRequest(
            [Frozen] Mock<IRepository<Partner>> partnersRepositoryMock,
            SetPartnerPromoCodeLimitRequest request,
            [NoAutoProperties] PartnersController partnersController)
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            Partner partner = new PartnerBuilder().Build();

            partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>()
                .Which.Value.Should().BeEquivalentTo("Данный партнер не активен");
        }

        [Theory]
        [AutoDomainData]
        public async void SetPartnerPromoCodeLimitAsync_IfRequestLimitLessThanZero_RetunBadRequest(
            [Frozen] Mock<IRepository<Partner>> partnersRepositoryMock,
            SetPartnerPromoCodeLimitRequest request,
            [NoAutoProperties] PartnersController partnersController)
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            Partner partner = new PartnerBuilder().Build();
            request.Limit = 0;

            partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>()
                .Which.Value.Should().BeEquivalentTo("Лимит должен быть больше 0");
        }

        [Theory]
        [AutoDomainData]
        public async void SetPartnerPromoCodeLimitAsync_AddingNewLimit_ShouldSaveNewLimitToDb(
            SetPartnerPromoCodeLimitRequest request)
        {
            // Arrange
            var partner = new PartnerBuilder().Activate().AddIssuedPromocodes().Build();
            using var context = new DataContext(_databaseFixture.Options);
            var partnersRepository = new EfRepository<Partner>(context);
            var partnersBussines = new PartnersBussines(partnersRepository);
            var partnersController = new PartnersController(partnersRepository, partnersBussines);

            request.Limit = 100;

            await partnersRepository.AddAsync(partner);

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            var value = result.Should().BeAssignableTo<CreatedAtActionResult>().Subject.RouteValues;

            value["id"].Should().NotBeNull()
                .And.Be(partner.Id);

            Partner dbPartner = await partnersRepository.GetByIdAsync((Guid)value["id"]);
            dbPartner.Should().NotBeNull();

            PartnerPromoCodeLimit partnerLimit = dbPartner.PartnerLimits.LastOrDefault();
            partnerLimit.Should().NotBeNull();
            partnerLimit.Limit.Should().Be(request.Limit);
            partnerLimit.EndDate.Should().Be(request.EndDate);
            partnerLimit.CreateDate.Should().BeCloseTo(DateTime.Now, 100);
            partnerLimit.PartnerId.Should().Be(partner.Id);
            partnerLimit.Id.Should().Be((Guid)value["limitId"]);
        }
    }
}