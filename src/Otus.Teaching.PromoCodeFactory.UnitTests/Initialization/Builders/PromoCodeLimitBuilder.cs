﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Initialization.Builders
{
    public class PromoCodeLimitBuilder
    {
        private PartnerPromoCodeLimit _partnerPromoCodeLimit;

        public PromoCodeLimitBuilder()
        {
            _partnerPromoCodeLimit = new PartnerPromoCodeLimit()
            {
                Id = Guid.NewGuid(),
                CreateDate = DateTime.Now
            };
        }

        public PromoCodeLimitBuilder AddPartner(Guid id)
        {
            _partnerPromoCodeLimit.PartnerId = id;

            return this;
        }

        public PromoCodeLimitBuilder AddEndDate(DateTime endDate = default)
        {
            if (endDate == default)
                endDate = DateTime.Now.AddDays(100);

            _partnerPromoCodeLimit.EndDate = endDate;

            return this;
        }

        public PromoCodeLimitBuilder AddLimit(int limit = 100)
        {
            _partnerPromoCodeLimit.Limit = limit;

            return this;
        }

        public PartnerPromoCodeLimit Build()
        {
            return _partnerPromoCodeLimit;
        }
    }
}
