﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Bussines.Interfaces
{
    public interface IPartnersBussines
    {
        Task CancelPartnersLimitAsync(Partner partner);
        Task<object> CreateNewPartnerLimitAsync(Partner partner, SetPartnerPromoCodeLimitRequest request);
    }
}
